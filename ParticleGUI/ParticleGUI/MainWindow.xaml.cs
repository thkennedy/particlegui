﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;


namespace ParticleGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // class variables
        ImageBrush TextureCanvasBrush = new ImageBrush();

        Process myProc;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Gravity_Direction_X_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Gravity_CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Gravity_Direction_X_TextBox.IsEnabled = true;
            Gravity_Direction_Y_TextBox.IsEnabled = true;
            Gravity_Direction_Z_TextBox.IsEnabled = true;

        }

        private void Gravity_CheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            Gravity_Direction_X_TextBox.IsEnabled = false;
            Gravity_Direction_Y_TextBox.IsEnabled = false;
            Gravity_Direction_Z_TextBox.IsEnabled = false;
        }

        private void Browse_Button_OnClick(object sender, RoutedEventArgs e)
        {
            // openfile dialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // filter for file extensions/default file extension
            dlg.DefaultExt = ".png, .jpg";
            dlg.Filter = "PNG (.png)|*.png|DDS (.dds)|*.dds | JPG (.jpg)|*.jpg|BMP (.bmp)|*.bmp | All File Types (*.*)|*.*";

            // display openfiledialog by calling showdialog method
            Nullable<bool> result = dlg.ShowDialog();

            // get the selected file name and diplay in the text box
            if (result == true)
            {
                // open document
                string filename = dlg.FileName;
                Texture_TextBox.Text = filename;
                // change the picture displayed
                TextureCanvasBrush.ImageSource = new BitmapImage(new Uri(Texture_TextBox.Text));

                Texture_Rectangle.Fill = TextureCanvasBrush;
            }
           

            
        }

        private void Texture_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            
        }

        private void FX_FileName_Button_OnClick(object sender, RoutedEventArgs e)
        {
            // openfile dialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // filter for file extensions/default file extension
            dlg.DefaultExt = ".fx";
            dlg.Filter = "FX File (.fx)|*.fx|HLSL File (.hlsl)|*.hlsl";

            // display openfiledialog by calling showdialog method
            Nullable<bool> result = dlg.ShowDialog();

            // get the selected file name and siplay in a text box
            if (result == true)
            {
                // open document
                string filename = dlg.FileName;
                FX_Name_TextBox.Text = filename;
            }
        }

        private void Export_Button_OnClick(object sender, RoutedEventArgs e)
        {
            // collect all data
            string[] data = {Texture_TextBox.Text,
                                Particle_Lifetime_TextBox.Text,
                                Gravity_Direction_X_TextBox.Text,
                                Gravity_Direction_Y_TextBox.Text,
                                Gravity_Direction_Z_TextBox.Text,
                                Orientation_X_TextBox.Text,
                                Orientation_Y_TextBox_.Text,
                                Orientation_Z_TextBox.Text,
                                FX_Name_TextBox.Text,
                                Technique_Name_TextBox.Text,
                                Max_Num_Particles_TextBox.Text,
                                Blend_Color_R.Text,
                                Blend_Color_G.Text,
                                Blend_Color_B.Text
                            };

            System.IO.File.WriteAllLines("Output.txt", data);
                       
        }

        private void Preview_Button_OnClick(object sender, RoutedEventArgs e)
        {
            myProc = Process.Start("BardCore.exe");
        }

    }
}
